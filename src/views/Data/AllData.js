import React, { PureComponent } from 'react'
import { services } from '../../services/Services';
import { commonFuncs } from '../../utils/Functions';
import SearchBox from '../../utils/SearchBox';
import Table from '../../utils/Table/Table';
import Actions from './Actions';
import _cloneDeep from 'lodash/cloneDeep';
import { constants } from '../../Constants';
import withDataContext from '../../HOC/withDataContext';
import PaginationModel from '../../model/PaginationModel';

class AllData extends PureComponent {

   constructor(props) {
      super(props)
      this.state = {
         data: [],
         columns: [],
         isLoading: false,
         searchedValue: '',
         pagination: new PaginationModel(1, 0),
      }
      this.mounted = false;
   }

   componentDidMount() {
      this.mounted = true;
      this.prepareColumns();
      if (this.props.searchedValue.trim() === '' || this.props.data.length === 0) {
         this.getData('', 1);
      }
      else {
         this.setState({
            data: this.props.data,
            searchedValue: this.props.searchedValue,
            pagination: this.props.pagination,
         })
      }
   }

   formmatTitle = (cell, row) => {
      return <span className={`animation-data${row.removed ? '--disabled' : ''}`}>{cell}</span>
   }

   formmatDate = (cell, row) => {
      return <span className={`animation-data${row.removed ? '--disabled' : ''}`}>{commonFuncs.convertToDateTime(cell)}</span>
   }

   handleLike = (nasa_id) => {
      this.handleData(constants.LIKE, nasa_id)
   }

   handleUnlike = (nasa_id) => {
      this.handleData(constants.UNLIKE, nasa_id)
   }

   handleRemove = (nasa_id) => {
      this.handleData(constants.REMOVE, nasa_id)
   }

   handleRestore = (nasa_id) => {
      this.handleData(constants.RESTORE, nasa_id)
   }

   handleData = (type, nasa_id) => {
      if (this.state.data && this.state.data.length !== 0) {
         let clonedData = _cloneDeep(this.state.data);
         for (let i = 0; i < clonedData.length; i++) {
            let currentData = clonedData[i];
            if (currentData.data[0].nasa_id === nasa_id) {
               if (type === constants.LIKE) {
                  currentData.liked = true;
               } else if (type === constants.UNLIKE) {
                  currentData.liked = false;
               } else if (type === constants.REMOVE) {
                  currentData.removed = true;
               } else if (type === constants.RESTORE) {
                  currentData.removed = false;
               }
            }
         }
         this.props.updateData(clonedData);
         this.setState({
            data: clonedData
         })
      }
   }

   actionData = (cell, row) => {
      return <Actions
         rowData={row}
         handleLike={this.handleLike}
         handleUnlike={this.handleUnlike}
         handleRemove={this.handleRemove}
         handleRestore={this.handleRestore}
      />
   }

   prepareColumns = () => {
      this.setState({
         columns: [
            {
               header: 'Title',
               cell: 'data[0].title',
               formatter: this.formmatTitle
            },
            {
               header: 'Date created',
               cell: 'data[0].date_created',
               formatter: this.formmatDate
            },
            {
               header: 'Action',
               cell: '',
               cellClass: 'last-col',
               headerClass: 'last-header-col',
               formatter: this.actionData
            },
         ]
      })
   }

   getData = async (value, page) => {
      await this.setState({
         isLoading: true,
      })
      let currentPage = this.state.pagination.page;
      if (page) {
         currentPage = page
      }
      let data = await services.searchData(value, currentPage);
      console.log(data)
      if (this.mounted) {
         if (data.collection) {
            this.setState({
               searchedValue: value,
               isLoading: false,
               data: data.collection.items,
               pagination: {
                  page: page,
                  totalRows: data.collection.metadata.total_hits,
               }
            })
            commonFuncs.setLocalStorage('data', data.collection.items)
            this.props.updateData(data.collection.items);
            this.props.setSearchedValue(value);
            this.props.updatePagination(new PaginationModel(page, data.collection.metadata.total_hits));
         }
         else {
            this.setState({
               isLoading: false,
               data: [],
               pagination: {
                  page: 1,
                  totalRows: 0,
               }
            })
            commonFuncs.setLocalStorage('data', [])
            this.props.updateData([]);
            this.props.setSearchedValue('');
            this.props.updatePagination(new PaginationModel(0, 0));
         }
      }
   }

   handleSearch = (value) => {
      if (!this.state.isLoading) {
         this.getData(value, this.state.pagination.page)
      }
   }

   handlePagination = (pagination) => {
      this.getData(this.state.searchedValue, pagination.page);
   }

   componentWillUnmount() {
      this.mounted = false;
   }

   render() {
      return (
         <div className="all-data">
            <SearchBox
               value={this.state.searchedValue}
               onSearch={this.handleSearch}
            />
            <div className="all-data__table">
               <Table
                  rowKey="data[0].nasa_id"
                  data={this.state.data}
                  columns={this.state.columns}
                  isLoading={this.state.isLoading}
                  onPaginate={this.handlePagination}
                  pagination={{
                     page: this.state.pagination.page,
                     totalRows: this.state.pagination.totalRows,
                  }}
               />
            </div>
         </div>
      )
   }
}

export default withDataContext(AllData);