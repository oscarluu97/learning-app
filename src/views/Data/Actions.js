import React, { PureComponent } from 'react'
import { ReactComponent as IcLike } from '../../assets/icons/ic-like.svg'
import { ReactComponent as IcRemove } from '../../assets/icons/ic-remove.svg'
import { ReactComponent as IcUndo } from '../../assets/icons/ic-undo.svg'
import Tooltip from '../../utils/Tooltip';

class Actions extends PureComponent {

   render() {
      const { rowData, handleLike, handleUnlike, handleRemove, handleRestore } = this.props;
      return (
         <div className="group-action animation-data">
            {
               !rowData.removed &&
               <Tooltip title={`${!rowData.liked ? 'Like' : 'Unlike'}`}>
                  <div className={`common-btn ${!rowData.liked ? '' : 'btn-unlike'}`} onClick={!rowData.liked ? () => handleLike(rowData.data[0].nasa_id) : () => handleUnlike(rowData.data[0].nasa_id)}>
                     <IcLike />
                  </div>
               </Tooltip>
            }
            {
               !rowData.removed &&
               <Tooltip title="Remove">
                  <div className="common-btn" onClick={() => handleRemove(rowData.data[0].nasa_id)}>
                     <IcRemove />
                  </div>
               </Tooltip>
            }
            {
               rowData.removed &&
               <Tooltip title="Undo">
                  <div className="common-btn" onClick={() => handleRestore(rowData.data[0].nasa_id)}>
                     <IcUndo />
                  </div>
               </Tooltip>
            }
         </div>
      )
   }
}

export default Actions;
