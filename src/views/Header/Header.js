import React, { PureComponent } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { constants } from '../../Constants'

const LIST_TAB = [
   {
      tabKey: 'all-data',
      tabName: 'All data',
      link: constants.ROUTE_DEFAULT,
   },
   {
      tabKey: 'liked-data',
      tabName: 'Liked data',
      link: constants.ROUTE_LIKED_DATA,
   },
   {
      tabKey: 'removed-data',
      tabName: 'Removed data',
      link: constants.ROUTE_REMOVED_DATA,
   },
]
class Header extends PureComponent {

   checkActivedLink = (link) => {
      let flag = false;
      if (this.props.location.pathname === link) {
         flag = true;
      }
      return flag;
   }

   render() {
      return (
         <div className="header-main">
            <div className="header-main__content">
               {
                  LIST_TAB.map(tab => {
                     return (
                        <Link
                           to={tab.link}
                           key={tab.tabKey}
                           className={`header-main__tab${this.checkActivedLink(tab.link) ? '--actived' : ''}`}
                        >
                           {tab.tabName}
                        </Link>
                     )
                  })
               }
            </div>
         </div>
      )
   }
}

export default withRouter(Header);
