import React, { PureComponent } from 'react'
import withDataContext from '../../HOC/withDataContext';
import { commonFuncs } from '../../utils/Functions';
import Table from '../../utils/Table/Table';

class LikedData extends PureComponent {

   state = {
      data: [],
      columns: [],
   }

   formmatDate = (cell, row) => {
      return commonFuncs.convertToDateTime(cell);
   }

   prepareColumns = () => {
      this.setState({
         columns: [
            {
               header: 'Title',
               cell: 'data[0].title',
            },
            {
               header: 'Date created',
               cell: 'data[0].date_created',
               formatter: this.formmatDate
            },
         ]
      })
   }

   prepareData = (data) => {
      let newData = [];
      for (const itemData of data) {
         if (itemData.liked) {
            newData.push(itemData);
         }
      }
      return newData;
   }

   componentDidMount() {
      this.prepareColumns();
      if (this.props.data && this.props.data.length !== 0) {
         this.setState({
            data: this.prepareData(this.props.data)
         })
      }
   }

   render() {
      return (
         <div className="all-data">
            <div className="all-data__table">
               <Table
                  rowKey="data[0].nasa_id"
                  data={this.state.data}
                  columns={this.state.columns}
                  isLoading={this.state.isLoading}
               />
            </div>
         </div>
      )
   }
}

export default withDataContext(LikedData);
