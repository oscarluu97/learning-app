import React from "react";
const DataContext = React.createContext({
   searchedValue: '',
   data: [],
   pagination: null,
   updateData: () => { },
   setSearchedValue: () => { },
   updatePagination: () => { },
});

export default DataContext;