const ROUTE_DEFAULT = '/';
const ROUTE_LIKED_DATA = '/liked-data';
const ROUTE_REMOVED_DATA = '/removed-data';

//For random key
const LETTER_LIST = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const NUMBER_LIST = '1234567890';
const WORD_LIST = LETTER_LIST + NUMBER_LIST;

const defaultTimeZone = 'Asia/Ho_Chi_Minh';
const dateTimeFormat = 'DD/MM/YYYY HH:mm';

const LIKE = 'like';
const UNLIKE = 'unlike';
const REMOVE = 'remove';
const RESTORE = 'restore';

export const constants = {
   ROUTE_DEFAULT,
   ROUTE_LIKED_DATA,
   ROUTE_REMOVED_DATA,
   WORD_LIST,
   defaultTimeZone,
   dateTimeFormat,
   LIKE,
   UNLIKE,
   REMOVE,
   RESTORE
}