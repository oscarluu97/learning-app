import fetch from 'cross-fetch';
import { API_CONST } from '../services/Constants';

const searchData = async (text = '', page = 1) => {
   try {
      let getObject = {
         method: 'GET',
         headers: {
            'Content-Type': 'application/json',
         }
      };
      let url = API_CONST.GET_SEARCHED_DATA + `?q=${encodeURIComponent(text ? text : ' ')}&page=${encodeURIComponent(page)}`;
      let response = await fetch(url, getObject);
      const body = await response.json();
      return body;
   } catch (error) {
      console.log(error)
   }
}

const getAssets = async (nasa_id) => {
   try {
      let getObject = {
         method: 'GET',
         headers: {
            'Content-Type': 'application/json',
         }
      };

      let url = API_CONST.GET_ASSET + `${nasa_id ? `/${nasa_id}` : ''}`
      let response = await fetch(url, getObject);
      const body = await response.json();
      return [body.errorCode === 0, body];

   } catch (error) {
      console.log(error)
   }
}

const getMetadata = async (nasa_id) => {
   try {
      let getObject = {
         method: 'GET',
         headers: {
            'Content-Type': 'application/json',
         }
      };

      let url = API_CONST.GET_METADATA + `${nasa_id ? `/${nasa_id}` : ''}`
      let response = await fetch(url, getObject);
      const body = await response.json();
      return [body.errorCode === 0, body];

   } catch (error) {
      console.log(error)
   }
}

export const services = {
   searchData,
   getAssets,
   getMetadata,
}