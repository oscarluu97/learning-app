const API_ROOT = 'https://images-api.nasa.gov';

const GET_SEARCHED_DATA = API_ROOT + '/search';
const GET_ASSET = API_ROOT + '/asset';
const GET_METADATA = API_ROOT + '/metadata';
const GET_CAPTIONS = API_ROOT + '/captions';

export const API_CONST = {
   API_ROOT,
   GET_SEARCHED_DATA,
   GET_ASSET,
   GET_METADATA,
   GET_CAPTIONS,
}