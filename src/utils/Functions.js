import _random from 'lodash/random';
import moment from "moment-timezone";
import { constants } from '../Constants';

/**
 * Random
 * @param {*} source source to generate key.
 * @param {*} length the length of key.
 */
const randomSequenceCharacters = (source, length) => {
   let result = '';
   for (let i = 0; i < length; i++) {
      let number = _random(0, length - 1);
      result = `${result}${source.charAt(number)}`;
   }
   return result;
}

const convertToDateTime = (ISOString, dateTimeFormat, timeZone) => {
   let dataUtil = moment.utc(ISOString).toDate();
   return moment(dataUtil).tz(timeZone || constants.defaultTimeZone).format(dateTimeFormat || constants.dateTimeFormat);
}

const setLocalStorage = (name, value) => {
   localStorage.setItem(name, JSON.stringify(value));
}

const getLocalStorage = (name) => {
   return localStorage.getItem(name);
}

export const commonFuncs = {
   randomSequenceCharacters,
   convertToDateTime,
   setLocalStorage,
   getLocalStorage
}

