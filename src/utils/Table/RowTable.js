import React, { PureComponent } from 'react';
import CellTable from './CellTable';

class RowTable extends PureComponent {

   render() {
      const { columns } = this.props;
      return (
         <tr className="table-row">
            {
               columns.map((col, index) => {
                  return (
                     <td
                        className={`super-col ${col.cellClass ? col.cellClass : ''}`}
                        key={`${col.name}_${index}`}
                     >
                        <CellTable
                           {...this.props}
                           col={col}
                        />
                     </td>
                  )
               })
            }
         </tr>
      )
   }
}

export default RowTable;
