import React, { PureComponent } from 'react'
import PaginationModel from '../../model/PaginationModel';
import { ReactComponent as IcNext } from '../../assets/icons/right-arrow.svg'
import { ReactComponent as IcPrevious } from '../../assets/icons/left-arrow.svg'
import _isEqual from 'lodash/isEqual';

const DEFAULT_PAGE_SIZE = 100;
class Pagination extends PureComponent {

   constructor(props) {
      super(props)
      this.state = {
         value: 0,
         pagination: new PaginationModel(0, 0),
      }

      this.refInput = null;
   }

   componentDidMount() {
      if (this.props.pagination instanceof PaginationModel) {
         this.setState({
            pagination: this.props.pagination,
            value: this.props.pagination.page,
         })
      }
   }

   componentDidUpdate(prevProps, prevState) {
      if (!_isEqual(prevProps.pagination, this.props.pagination)) {
         this.setState({
            pagination: this.props.pagination,
            value: this.props.pagination.page,
         })
      }
   }

   handleChange = (event) => {
      this.setState({
         value: event.target.value
      })
   }

   handleMouseDown = (event) => {
      if (event.key === 'Enter') {
         const totalPages = Math.ceil(this.state.pagination.totalRows / DEFAULT_PAGE_SIZE) || 0;
         const exceptedTotals = totalPages ? (totalPages > 100 ? 100 : totalPages) : 0;
         if (this.state.value >= 1 && this.state.value <= exceptedTotals) {
            this.props.onPaginate({ page: this.state.value });
            this.refInput.blur();
         }
      }
   }

   handlePrevious = () => {
      const previousPage = this.state.pagination.page - 1;
      if (previousPage > 0) {
         this.setState({
            value: previousPage
         })
         this.props.onPaginate({ page: previousPage });
      }
   }

   handleNext = () => {
      const nextPage = this.state.pagination.page + 1;
      const totalPages = Math.ceil(this.state.pagination.totalRows / DEFAULT_PAGE_SIZE) || 0;
      const exceptedTotals = totalPages ? (totalPages > 100 ? 100 : totalPages) : 0;
      if (nextPage < exceptedTotals) {
         this.setState({
            value: nextPage
         })
         this.props.onPaginate({ page: nextPage });
      }
   }

   render() {
      const totalPages = Math.ceil(this.state.pagination.totalRows / DEFAULT_PAGE_SIZE) || 0;
      const exceptedTotals = totalPages ? (totalPages > 100 ? 100 : totalPages) : 0;
      const isValidInput = Number(this.state.value) > totalPages || Number(this.state.value) < 0;
      const disabledNext = this.state.pagination.page >= totalPages;
      const disabledPrevious = this.state.pagination.page <= 1 || totalPages === 0;

      return (
         <div className="table-pagination">
            <div>Totals : <span>{exceptedTotals}</span></div>
            <div className="table-pagination__next-previous">
               <div className={`btn-common${disabledPrevious ? '--disabled' : ''}`} onClick={this.handlePrevious}>
                  <IcPrevious />
               </div>
               <input
                  ref={ref => this.refInput = ref}
                  name="value"
                  value={this.state.value}
                  className={`input-pagination${isValidInput ? '__error' : ''}`}
                  onChange={this.handleChange}
                  onKeyDown={this.handleMouseDown}
               />
               <div className={`btn-common${disabledNext ? '--disabled' : ''}`} onClick={this.handleNext}>
                  <IcNext />
               </div>
            </div>
         </div>
      )
   }
}

export default Pagination;
