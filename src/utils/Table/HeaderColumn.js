import React, { PureComponent } from 'react'

class HeaderColumn extends PureComponent {
   constructor(props) {
      super(props);
      this.state = {
         sortType: '',
      }
   }

   componentDidMount() {
      const { sortObj, col } = this.props;
      if (sortObj) {
         if (sortObj.orderBy === col.name) {
            this.setState({
               sortType: sortObj.sortType,
            });
         }
      }
   }

   componentDidUpdate(prevProps) {
      if (this.props.sortObj && prevProps.sortObj !== this.props.sortObj) {
         if (this.props.sortObj.orderBy === this.props.col.name) {
            this.setState({
               sortType: this.props.sortObj.sortType,
            });
         } else {
            this.setState({
               sortType: '',
            });
         }
      }
   }

   getSortType = (currentType) => {
      if (currentType === 'DESC') {
         return 'ASC';
      } else if (currentType === 'ASC') {
         return 'DESC';
      }
      else {
         return 'DESC';
      }
   }

   sortColumn = () => {
      const { col } = this.props;
      const { sortType } = this.state;
      if (col.sort) {
         let sortObj = {
            orderBy: col.name,
            sortType: this.getSortType(sortType),
         };
         this.setState({
            sortType: sortObj.sortType,
         });
         this.props.onSort(sortObj);
      }
   }

   render() {
      const { col } = this.props;
      const { sortType } = this.state;
      return (
         <span className="header-column">
            <span className={`${col.sort ? 'label-sort-col' : ''} ${sortType === 'DESC' ? 'sort-col-desc' : `${sortType === 'ASC' ? 'sort-col-asc' : ''}`}`}
               onClick={this.sortColumn}
            >
               {col.header}
               {
                  col.sort &&
                  <i className="icon-size-16 ic-sort-col" />
               }
            </span>
         </span>
      );
   }
}

export default HeaderColumn;
