import React, { PureComponent } from 'react'
import HeaderColumn from './HeaderColumn';
import RowTable from './RowTable';
import _cloneDeep from 'lodash/cloneDeep';
import _isEqual from 'lodash/isEqual';
import _get from 'lodash/get';
import { commonFuncs } from '../../utils/Functions';
import { constants } from '../../Constants';
import { ReactComponent as IconLoading } from '../../assets/icons/ic-loading.svg';
import Pagination from './Pagination'

const defaultMessageNoData = 'No Data';

class Table extends PureComponent {

   constructor(props) {
      super(props);
      this.state = {
         sortObj: {
            orderBy: '',
            sortType: '',
         }
      }
   }

   loadDataTable = () => {
      const { data } = this.props;
      if (!_isEqual(data, this.state.data)) {
         this.setState({ data: _cloneDeep(this.props.data) });
      }
   }

   componentDidUpdate(prevProps, prevState) {
      if (prevProps.sortObj !== this.props.sortObj) {
         this.setState({
            sortObj: this.props.sortObj
         })
      }
   }

   handleSortColumn = (sortObj) => {
      this.setState({
         sortObj: _cloneDeep(sortObj),
      });
      if (this.props.onSort) {
         this.props.onSort(sortObj);
      }
   }

   handleGetKey = (rowData, rowKey) => {
      return _get(rowData, rowKey) ? _get(rowData, rowKey) : '';
   }

   render() {
      const { columns, data, rowKey, classNameTable, messageNoData, pagination, isLoading } = this.props;
      return (
         <div className={`table-responsive ${isLoading ? 'table-disabled' : ''}`}>
            <table className={`container-table ${classNameTable ? classNameTable : ''}`}>
               <thead>
                  <tr className="table-row-header">
                     {
                        columns &&
                        columns.map((col, index) => {
                           return (
                              <th
                                 key={`${col}_${index}`}
                                 className={`header-col ${index === 0 ? 'first-col' : ''} ${index === (columns.length - 1) ? 'last-col' : ''} ${col.headerClass || ''}`}
                              >
                                 <HeaderColumn
                                    col={col}
                                    sortObj={this.state.sortObj}
                                    onSort={this.handleSortColumn}
                                 />
                              </th>
                           )
                        })
                     }
                  </tr>
               </thead>
               <tbody>
                  {
                     data &&
                     data.length !== 0 &&
                     data.map((row, index) => {
                        return (
                           <RowTable
                              key={this.handleGetKey(row, rowKey) ? this.handleGetKey(row, rowKey) : commonFuncs.randomSequenceCharacters(constants.WORD_LIST, 20)}
                              rowKey={rowKey}
                              rowData={row}
                              columns={columns}
                           />
                        )
                     })
                  }
                  {
                     data.length === 0 &&
                     !isLoading &&
                     <tr>
                        <td className="table-row" colSpan={columns.length}>
                           <div className="super-row-no-data">{messageNoData || defaultMessageNoData}</div>
                        </td>
                     </tr>
                  }
               </tbody>
            </table>
            {
               isLoading &&
               <div className="table-loading">
                  <IconLoading />
               </div>
            }
            {
               pagination &&
               this.props.onPaginate &&
               <Pagination
                  pagination={pagination}
                  onPaginate={this.props.onPaginate}
               />
            }
         </div>
      )
   }
}

export default Table;
