import React, { PureComponent } from 'react'

export default class SearchBox extends PureComponent {

   state = {
      value: ''
   }

   timerSearch = null;

   refInput = React.createRef();

   componentDidMount() {
      if (this.props.value) {
         this.setState({
            value: this.props.value,
         })
      }
   }

   componentDidUpdate(prevProps, prevState) {
      if (prevProps.value !== this.props.value) {
         this.setState({
            value: this.props.value,
         })
      }
   }

   setRefInput = (ref) => {
      this.refInput = ref;
   }

   doSearch = (value) => {
      if (this.props.onSearch) {
         this.props.onSearch(value)
      }
   }

   handleChange = (event) => {
      clearTimeout(this.timerSearch);
      this.setState({
         [event.target.name]: event.target.value
      })
      this.timerSearch = setTimeout(() => {
         this.doSearch(event.target.value);
      }, 500);
   }

   hanleKeyDown = (event) => {
      if (event.key === 'Enter') {
         this.refInput.blur();
         clearTimeout(this.timerSearch)
         this.props.onSearch(this.state.value)
      }
   }

   componentWillUnmount() {
      clearTimeout(this.timerSearch);
      this.timerSearch = null;
   }

   clearBox = () => {
      clearTimeout(this.timerSearch)
      this.setState({
         value: '',
      })
      this.props.onSearch('')
   }

   render() {
      return (
         <div className="search-box-main">
            <svg
               width={16}
               height={16}
               viewBox="0 0 16 16"
               fill="none"
               xmlns="http://www.w3.org/2000/svg"
               className="ic-search"
            >
               <circle cx={7} cy={7} r="5.5" stroke="black" strokeOpacity="0.4" />
               <path d="M14.6464 15.3536C14.8417 15.5488 15.1583 15.5488 15.3536 15.3536C15.5488 15.1583 15.5488 14.8417 15.3536 14.6464L14.6464 15.3536ZM15.3536 14.6464L11.3536 10.6464L10.6464 11.3536L14.6464 15.3536L15.3536 14.6464Z" fill="black" fillOpacity="0.4" />
            </svg>
            <input
               placeholder="Search here"
               ref={this.setRefInput}
               name="value"
               value={this.state.value}
               onKeyDown={this.hanleKeyDown}
               onChange={this.handleChange}
            />
            {
               this.state.value.trim() !== '' &&
               <div className="btn-cancel" onClick={this.clearBox}>
                  <svg
                     width={16}
                     height={16}
                     viewBox="0 0 16 16"
                     fill="none"
                     xmlns="http://www.w3.org/2000/svg"
                  >
                     <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M16 8C16 12.4183 12.4183 16 8 16C3.58172 16 0 12.4183 0 8C0 3.58172 3.58172 0 8 0C12.4183 0 16 3.58172 16 8ZM8 7.29289L10.6464 4.64645L11.3536 5.35355L8.70711 8L11.3536 10.6464L10.6464 11.3536L8 8.70711L5.35355 11.3536L4.64645 10.6464L7.29289 8L4.64645 5.35355L5.35355 4.64645L8 7.29289Z"
                        fill="#AAA9B7"
                     />
                  </svg>
               </div>
            }
         </div>
      )
   }
}
