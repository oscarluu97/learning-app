import React, { PureComponent } from 'react'

export default class ScrollToTop extends PureComponent {
   componentDidUpdate(prevProps) {
      if (this.props.location.pathname !== prevProps.location.pathname) {
         window.scrollTo(0, 0);
      }
      if (this.props.location.search !== prevProps.location.search) {
         window.scrollTo(0, 0);
      }
   }
   render() {
      return this.props.children;
   }
}
