import React, { Fragment, PureComponent } from 'react'
import { Tooltip as RsTooltip } from 'reactstrap'

class Tooltip extends PureComponent {

   state = {
      isOpen: false,
   }

   childRef = null;

   setChildRef = (r) => {
      this.childRef = r;
   }

   toggle = () => {
      this.setState({
         isOpen: !this.state.isOpen,
      })
   }

   showTooltip = () => {
      if (!this.state.isOpen) {
         this.setState({ isOpen: true });
      }
   }

   doMouseOver = () => {
      this.showTooltip();
   }

   render() {
      const { placement = 'bottom', title, children, className } = this.props;
      return (
         <Fragment>
            {
               React.Children.map(children,
                  child => {
                     return (
                        React.cloneElement(child, {
                           ref: (node) => {
                              // Keep your own reference
                              this.childRef = node;
                              // Call the original ref, if any
                              const { ref } = child;
                              if (typeof ref === 'function') {
                                 ref(node);
                              }
                           },
                           onMouseOver: this.doMouseOver,
                        })
                     )
                  }
               )
            }
            {
               this.state.isOpen &&
               <RsTooltip
                  placement={placement}
                  className={`${className || ''} common-tooltip`}
                  isOpen={this.state.isOpen}
                  autohide={false}
                  target={this.childRef}
                  toggle={this.toggle}
               >
                  {title}
               </RsTooltip>
            }

         </Fragment>
      )
   }
}

export default Tooltip;
