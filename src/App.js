import React, { PureComponent } from 'react'
import { HashRouter, Route, Switch } from 'react-router-dom'
import Loadable from 'react-loadable'
import { constants } from './Constants'
import LayoutLoading from './utils/LayoutLoading';
import ScrollToTop from './utils/ScrollToTop';
import Header from './views/Header/Header';
import DataStore from './store/DataStore';

const RemovedData = Loadable({
  loader: () => import('./views/RemovedData/RemovedData'),
  loading: LayoutLoading,
});

const LikedData = Loadable({
  loader: () => import('./views/LikedData/LikedData'),
  loading: LayoutLoading,
});

const DataPage = Loadable({
  loader: () => import('./views/Data/AllData'),
  loading: LayoutLoading,
});

class App extends PureComponent {
  render() {
    return (
      <HashRouter>
        <ScrollToTop>
          <Header />
          <DataStore>
            <Switch>
              <Route path={constants.ROUTE_LIKED_DATA} name="Liked data" component={LikedData} />
              <Route path={constants.ROUTE_REMOVED_DATA} name="Removed data" component={RemovedData} />
              <Route path={constants.ROUTE_DEFAULT} name="Home" component={DataPage} />
            </Switch>
          </DataStore>
        </ScrollToTop>
      </HashRouter>
    )
  }
}

export default App;