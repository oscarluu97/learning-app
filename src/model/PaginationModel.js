class PaginationModel {
   constructor(page, totalRows) {
      this.page = page;
      this.totalRows = totalRows;
   }
}

export default PaginationModel;