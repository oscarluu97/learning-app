import React, { PureComponent } from 'react'
import DataContext from '../context/DataContext';
import PaginationModel from '../model/PaginationModel';

class DataStore extends PureComponent {

   state = {
      data: [],
      searchedValue: '',
      pagination: new PaginationModel(0, 0),
   }

   setSearchedValue = (searchedValue = '') => {
      this.setState({
         searchedValue: searchedValue
      })
   }

   updateData = (data) => {
      if (data !== this.state.data) {
         this.setState({
            data: data
         })
      }
   }

   updatePagination = (pagination) => {
      if (pagination !== this.state.pagination) {
         this.setState({
            pagination: pagination
         })
      }
   }

   render() {
      console.log(this.state.data)
      return (
         <DataContext.Provider value={{
            searchedValue: this.state.searchedValue,
            pagination: this.state.pagination,
            setSearchedValue: this.setSearchedValue,
            data: this.state.data,
            updateData: this.updateData,
            updatePagination: this.updatePagination,
         }}>
            {this.props.children}
         </DataContext.Provider>
      )
   }
}

export default DataStore;
