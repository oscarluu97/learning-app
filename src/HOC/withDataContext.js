import React, { PureComponent } from "react";
import DataContext from "../context/DataContext";

const withDataContext = (WrappedComponent) => {
   return class DataContextHOC extends PureComponent {
      render() {
         return (
            <DataContext.Consumer>
               {context => (
                  <WrappedComponent
                     {...this.props}
                     searchedValue={context.searchedValue}
                     pagination={context.pagination}
                     data={context.data}
                     updateData={context.updateData}
                     setSearchedValue={context.setSearchedValue}
                     updatePagination={context.updatePagination}
                  />
               )}
            </DataContext.Consumer>
         )
      }
   }
}

export default withDataContext;